
# Installation
apt -y update && apt -y install git
cd /etc
git clone https://gitlab.com/parsunix.net/pnix.git
ln -s /etc/pnix/pnix /usr/bin/pnix


## How does it works.
1. You can use it manually ( pnix s cpanelhostuser )
2. or put it in crontab ( 1 1 * * * pnix s --ftp=ftpserver.net:ftp_user:ftp*pass*word:weekly  >/dev/null 2>&1 )

# cPanel
1. create backup:  pnix s [ --skip-gz --skip-home [ --skip-db / --mysqldump-all ] --plus-cool-db --ftp=server:user:pass:path ] username
2. restore backup: pnix r pnix-pars.tar.gz

# RAW
1. pnix m --dir=/home/dir_one:/home/dir_two --db=my_db:second_db:third_db --skip-gz --ftp=server:user:pass:path
2. pnix m --db=all --ftp=server:user:pass:path
 
# Flags
--skip-gz : skip gzip compression
--skip-home : skip making backup of homedir files 
--skip-db : skip making backup from mysql databases
--mysqldump-all : dump all mysql databases in .sql files separately [as an alternative backup of mysql]
--plus-cool-db : backup	all raw	binary database files placed on	/var/lib/mysql [usefull	when the mysql .sql files are not usable]
--ftp : for remote backup storage, server:user:pass:path
--cron : its a cron process, and it will not be interrupted

# Notes
1. it will dump only the sql files lower that 2KB
2. if --mysqldump-all it will dump all mysqls
3. if skip-db , mysqldump-all will not work
4. if skip-db and plus-cool-db it will only make the cool-db
5. /pnix_lock will be created when a backup process starts, and this file will lock the next backup processes till the current backup process ends.
6. take care about ~/.my.cnf on "pnix m"
